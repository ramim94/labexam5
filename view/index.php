<?php
include_once('../Student.php');
session_start();
$student=new Student();
$allstudents=$student->index();
?>

<html>
<head>
    <title>All Student Datalist</title>
</head>
<body>

    <h3>All Student Data</h3>

    <a href="create.php" role="button">Add New</a>

<div id="message">
    <?php echo $_SESSION['message'];
    $_SESSION['message']="";?>
</div>
<div>
    <table border="1">
        <thead>
        <tr>
            <th> SL NO</th>
            <th> ID NO</th>
            <th> First Name</th>
            <th> Middle Name</th>
            <th> Last Name</th>
            <th> Actions</th>
        </tr>
        </thead>
        <tbody>
<?php foreach($allstudents as $student){
    $sl=1 ; ?>
        <tr>
            <td> <?php echo $sl;?> </td>
            <td> <?php echo $student['id'];?> </td>
            <td> <?php echo $student['firstname'];?> </td>
            <td> <?php echo $student['middlename'];?> </td>
            <td> <?php echo $student['lastname'];?> </td>

            <td> <a role="button" href="view.php?id= <?php echo $student['id'];?> " >View</a>
                <a role="button"  href="edit.php?id= <?php echo $student['id'];?> " >Edit</a>
                <a role="button" href="delete.php?id= <?php echo $student['id'];?> ">Delete</a>
            </td>
        </tr>
        <?php }?>
        </tbody>
    </table>
</div>

<script>
    $('#message').fadeOut().time(2000);
</script>

</body>
</html>

