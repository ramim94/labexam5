<?php
include_once('../Student.php');
$student=new Student();
$student->prepare($_GET);
$singleStudent=$student->view();
?>

<html>
<head>
    <title>Student View</title>
</head>
<body>
<h3>Single Student:</h3>

<ul>
    <li> Student ID: <?php echo $singleStudent['id']?> </li>
    <li> Student First Name: <?php echo $singleStudent['firstname']?> </li>
    <li> Student Middle Name: <?php echo $singleStudent['middlename']?> </li>
    <li> Student Last Name: <?php echo $singleStudent['lastname']?> </li>
</ul>
</body>
</html>