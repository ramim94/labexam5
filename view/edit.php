<?php
include_once('../Student.php');

$student=new Student();
$student->prepare($_GET);
$singleStudent=$student->edit();

?>

<html>
<head>
    <title>Student View</title>
</head>
<body>
<h3>Single Student:</h3>

<form action="update.php" method="post">

    <input type="hidden" name="id" value="<?php echo $singleStudent['id']?> "> </input>

<p>Student Firstname: <input type="text" name="firstname" value="<?php echo $singleStudent['firstname']?> "></p>
<p>Student Middlename:   <input type="text" name="middlename" value="<?php echo $singleStudent['middlename']?> "></p>
<p>Student Lastname:   <input type="text" name="lastname" value="<?php echo $singleStudent['lastname']?> ">  </p>
<p> <input type="submit" value="Update"> </p>
</form>
</body>
</html>
